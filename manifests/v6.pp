# Tomcat version 6.

class tomcat::v6 inherits tomcat {
  # Pick the version of Java based on the distribution.
  case $::lsbdistcodename {
    'squeeze':  { include java::v6 }
    'wheezy':   { include packages::java_7 }
    default:    { include java::v6 }
  }

  # No implementation for Red Hat yet.
  case $::operatingsystem {
    'debian', 'ubuntu': {
      package {
        'tomcat6-user': ensure => present;
        'tomcat6':
          ensure  => present,
          require => $::lsbdistcodename ? {
            'squeeze' => Package['java6'],
            'wheezy'  => Package['java7'],
            default   => Package['java6'],
          };
      }

      # Replace the default init script with a fixed version that redirects
      # the output of rotatelogs to /dev/null.  Otherwise, newsyslog log
      # rotation doesn't work properly.
      file { '/etc/init.d/tomcat6':
        source  => 'puppet:///modules/tomcat/etc/init.d/tomcat6',
        mode    => '0755',
        require => Package['tomcat6'],
      }

      # Ensure the service is running.  If this works for RHEL, then move it
      # outside of this case statement.
      service { 'tomcat6':
        enable     => true,
        ensure     => running,
        hasstatus  => true,
        hasrestart => true,
        require    => Package['tomcat6'],
      }
    }
  }
}
