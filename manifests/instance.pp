# Create separate instances of tomcat in /usr/local/share/
# The provided name is prefixed with "tomcat-" to simplify sudoers config.
#
# NOTE: only tested with tomcat v6
#
# Requires Ubuntu or Debian with filesystems mounted with ACL support.
# ACLs are used w/ webconfig group for editing confs.
#
#
# tomcat::instance {
#   "webmaster":
#       name    => "webmaster",
#       version => '6';
#   "events":
#       name    => "events",
#       version => '6',
#       inittemplate => 'c_vppa/instance-init.erb';
# }
#
#   This creates /usr/local/share/tomcat-webmaster and tomcat-events and
# sets up a bunch of other required perms and files/dirs. The inittemplate is
# optional - the default should work for most cases.
#

define tomcat::instance(
    $ensure = "present",
    $inittemplate = 'tomcat/instance-init.erb',
    $name = "NOSRC",
    $newsyslog = true,
    $version = 6 )
{
    case $::operatingsystem {
        'redhat', 'centos': {
            fail ("tomcat::instance requires Debian or Ubuntu!")
        }
    }
    if ($name == "NOSRC") { fail ("tomcat::instance requires name!") }

    case $ensure {
        "present": {
            include group::webconfig,
                    packages::acl,
                    tomcat::instance::sudoers

            $tname = "tomcat-${name}"
            $uloc="/usr/local/share/${tname}"

            # this requires a tomcat module for the tomcat$version-user pkg
            exec { "create-instance ${tname}":
                command => "/usr/bin/tomcat${version}-instance-create ${uloc}",
                creates => "${uloc}/conf/server.xml",
                notify  => [    Exec["tomcat logs ${tname}"], File["${uloc}/work"],
                    File["${uloc}/conf"], File["${uloc}/temp"],
                    File["${uloc}/webapps"], Exec["setfacl ${tname}"],
                    Exec["copy policy.d ${tname}"] ],
                require => Package["tomcat${version}-user"],
            }

            # Note this doesn't set perms so users could edit - that would give
            # them root access since it is executed by the init script.
            exec { "cp /etc/default/tomcat${version} /etc/default/${tname}":
                creates => "/etc/default/${tname}",
            }

            # mv the log dir juse in case there is something in there - setfacl
            # to allow group webconfig to rwx
            exec { "tomcat logs ${tname}":
                command     => "bash -c \"chown tomcat${version} ${uloc}/logs && \
            mv ${uloc}/logs /var/log/${tname} && ln -s /var/log/${tname} ${uloc}/logs \
            && /usr/bin/setfacl -R -m g:webconfig:rwx /var/log/${tname} && \
            /usr/bin/setfacl -R -d -m g:webconfig:rwx /var/log/${tname}\"",
                creates     => "/var/log/${tname}",
                require     => Package['acl'],
            }

            # use ACLs to allow group webconfig to rwx
            exec { "setfacl ${tname}":
                command     => "bash -c \"/usr/bin/setfacl -R -m g:webconfig:rwx ${uloc} \
            && /usr/bin/setfacl -R -d -m g:webconfig:rwx ${uloc} && \
            echo 'This dir uses ACLs' >> ${uloc}/README.DirPermissions\"",
                require     => Package['acl'],
                creates     => "${uloc}/README.DirPermissions",
            }

            exec { "copy policy.d ${tname}":
                command     => "cp -r /etc/tomcat6/policy.d ${uloc}/conf/policy.d",
                creates     => "${uloc}/conf/policy.d",
            }

            file {
                "${uloc}/work":
                    ensure => directory,
                    mode   => 2775,
                    owner  => "tomcat${version}",
                    group  => "tomcat${version}";
                "${uloc}/conf":
                    ensure => directory,
                    mode   => 2775,
                    owner  => "tomcat${version}",
                    group  => "webconfig";
                "${uloc}/temp":
                    ensure => directory,
                    mode   => 2775,
                    owner  => "tomcat${version}";
                "${uloc}/webapps":
                    ensure => directory,
                    mode   => 2775,
                    owner  => "tomcat${version}",
                    group  => "webconfig";
                # soft link in lib to each location
                "${uloc}/lib":
                    ensure => "/usr/share/tomcat${version}/lib";
            }

            # init script from template
            file { "/etc/init.d/${tname}":
                content => template($inittemplate),
                require => Exec["create-instance $tname"],
                mode    => 0755,
                owner   => 'root',
                group   => 'root',
            }

            # enable the init script, but don't ensure running
            service { $tname:
                enable  => true,
                require => File["/etc/init.d/${tname}"],
            }

            # install newsyslog conf?
            if ($newsyslog == true) {
                # very basic newsyslog config
                file { "/etc/newsyslog.daily/${tname}":
                    content => template('tomcat/newsyslog.erb'),
                }
            } else {
                file { "/etc/newsyslog.daily/${tname}":
                    ensure => absent;
                }
            }
        }
        'absent': {
            service { $name:
                enable => false,
            }
            file { "/etc/newsyslog.daily/${tname}":
                ensure => absent,
            }
            file { "/etc/init.d/${tname}":
                ensure => absent,
            }
        }
        default: { crit "Invalid ensure value: ${ensure}" }
    }
}

class tomcat::instance::sudoers {
    # sudoers setup to allow group webconfig to execute tomcat-* init scripts
    base::textline { "#include /etc/sudoers.tomcat":
        ensure => "/etc/sudoers",
    }
    file { "/etc/sudoers.tomcat":
        source => "puppet:///modules/tomcat/etc/sudoers.tomcat",
        mode   => 0440,
        owner  => 'root',
        group  => 'root',
    }
}
