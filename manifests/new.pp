# Parameterized Tomcat class

class tomcat::new(
  $version=5,
  $javaversion=5,
  $arch='default',
  $javavendor='sun',
  $requirements='default',
  ) {

  if ($version == 6 and $javaversion < 6 ) {
    fail ("Tomcat version ${version} requires JAVA version later than version ${javaversion}")
  }

  class {'java::new':
    javaversion => $javaversion,
    vendor => $javavendor
    }

  case $::operatingsystem {
    'debian', 'ubuntu': {
      if ($arch != 'default' and $arch != '') {
        fail ("Arch parameter '$arch' not supported on $::operatingsystem")
      }
      package { "tomcat${version}":
        require => $requirements ? {
          'default' => [Package["java${javaversion}"]],
          default   => [[Package["java${javaversion}"]],$requirements]
        },
        ensure  => present;
      }
      case $version {
        '5','5.5','6': {
          # Replace the default init script with a fixed version that
          # redirects the output of rotatelogs to /dev/null.  Otherwise,
          # newsyslog log rotation doesn't work properly.
          file { "/etc/init.d/tomcat${version}":
            source  => "puppet:///modules/tomcat/etc/init.d/tomcat${version}",
            mode  => 755,
          }
        }
        default: {}
      }
    }
    'redhat': {
      case $arch {
        'i386','i686','x86_64','noarch': {$arch_suffix = ".${arch}"}
        'default',undef:                 {$arch_suffix = undef}
        default:                         { fail ("Invalid arch '$arch'") }
      }
      case $::lsbmajdistrelease {
        '3','4': {
          fail("Tomcat not available for RHEL$::lsbmajdistrelease")
        }
        default: {
          package {
            "tomcat${version}${arch_suffix}":
              alias  => "tomcat${version}",
              require => $requirements ? {
                'default' => [Package["java${javaversion}"]],
                default   => [[Package["java${javaversion}"]],$requirements]
              },
              ensure  => present;
          }
        }
      }
    }
  }

  # Ensure the service is running.
  service { "tomcat${version}":
    enable     => true,
    ensure     => running,
    hasstatus  => true,
    hasrestart => true,
    require    => Package["tomcat${version}"],
  }

  file { "/etc/filter-syslog/tomcat":
    source => "puppet:///modules/tomcat/etc/filter-syslog/tomcat",
  }
}
