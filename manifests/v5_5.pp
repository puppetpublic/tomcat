#
# Tomcat v5.5 classes

# By default, we require Java v5, since the init script only knows how to find
# that version of Java.  This should be revisited so that we can support Java
# v6 as well.
class tomcat::v5_5 inherits tomcat {
    include java::v5

    case $::operatingsystem {
        'debian', 'ubuntu': {
            package { "tomcat5.5": 
                ensure  => present,
                require => Package["java5"],
            }

            # Replace the default init script with a fixed version that
            # redirects the output of rotatelogs to /dev/null.  Otherwise,
            # newsyslog log rotation doesn't work properly.
            file { "/etc/init.d/tomcat5.5":
                source  => "puppet:///modules/tomcat/etc/init.d/tomcat5.5",
                mode    => 755,
                require => Package["tomcat5.5"],
            }
        }
    }
}
