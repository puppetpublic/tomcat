#
# Base class for Tomcat servers.  The actual work is done in subclasses
# specific to each Tomcat version.

# Only things common to all versions are done here.
class tomcat {
    file { "/etc/filter-syslog/tomcat":
        source => "puppet:///modules/tomcat/etc/filter-syslog/tomcat",
    }
}
